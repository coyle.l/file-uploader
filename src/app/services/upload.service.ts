import { Injectable } from "@angular/core";
import { HttpClient, HttpEventType } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class UploadService {
  SERVER_URL = "http://clients.priceveille.com/dossier_test_match/";

  constructor(private http: HttpClient) {}

  upload(formData): Observable<any> {
    return this.http.post(`${this.SERVER_URL}upload_csv.php`, formData, {
      reportProgress: true,
      observe: "events"
    }) as Observable<any>;
  }
  launch(): Observable<any> {
    return this.http.get(`${this.SERVER_URL}launch_csv.php`) as Observable<any>;
  }
}
