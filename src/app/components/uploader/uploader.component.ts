import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { UploadService } from "src/app/services/upload.service";
import { HttpEventType } from "@angular/common/http";

@Component({
  selector: "app-uploader",
  templateUrl: "./uploader.component.html",
  styleUrls: ["./uploader.component.scss"]
})
export class UploaderComponent implements OnInit {
  form: FormGroup;
  uploaded: Boolean = false;
  loading: Boolean = false;
  executed: Boolean = false;
  errorExe: Boolean = false;
  errorUpload: Boolean = false;
  uploadProgress: number = 0;
  executionMessage: string = "";
  nbProdsUnique: number = 0;
  nbProdsMaj: number = 0;
  nbElements: number = 0;

  uploadResponse = { status: "", message: "", filePath: "" };

  constructor(
    private formBuilder: FormBuilder,
    private uploadService: UploadService
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      file: [""]
    });
  }

  onFileChange(event) {
    this.uploadProgress = 0;
    this.uploaded = false;
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.form.get("file").setValue(file);
      console.log(this.form.get("file"));
    }
  }

  onSubmit() {
    let formData = new FormData();
    formData.append("file", this.form.get("file").value);

    this.uploadService.upload(formData).subscribe(events => {
      if (events.type == HttpEventType.UploadProgress) {
        console.log(
          "Upload progress: ",
          Math.round((events.loaded / events.total) * 100) + "%"
        );
        this.uploadProgress = Math.round((events.loaded / events.total) * 100);
      } else if (events.type === HttpEventType.Response) {
        this.uploaded = true;
        this.errorUpload = events.body.err;
        console.log(events);
      }
    });
  }

  launch() {
    this.executed = false;
    this.errorExe = false;
    this.loading = true;
    this.uploadService.launch().subscribe(data => {
      this.loading = false;
      if (data.err) {
        console.log("err execution", data.err);
        this.executed = true;
        this.executionMessage = "Erreur";
        this.errorExe = true;
      } else {
        this.executed = true;
        this.executionMessage = "Fini";
        this.nbElements = data.nb_elements;
        this.nbProdsUnique = data.nb_prods_unique;
        this.nbProdsMaj = data.nb_prods_maj;
      }
    });
  }
}
