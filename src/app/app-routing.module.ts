import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UploaderComponent } from "src/app/components/uploader/uploader.component";
import { AuthenticationGuard } from "src/app/guards/authentication.guard";

const routes: Routes = [
  {
    path: "",
    component: UploaderComponent,
    canActivate: [AuthenticationGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
